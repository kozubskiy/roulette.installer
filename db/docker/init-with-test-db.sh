#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
  CREATE DATABASE main;
  CREATE USER main WITH password 'main';
  GRANT ALL privileges ON DATABASE main TO main;

  CREATE DATABASE audit;
  CREATE USER audit WITH password 'audit';
  GRANT ALL PRIVILEGES ON DATABASE audit TO audit;

  CREATE DATABASE test;
  CREATE USER test WITH password 'test';
  GRANT ALL PRIVILEGES ON DATABASE test TO test;
EOSQL

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "main" <<-EOSQL
  CREATE EXTENSION "ltree";
EOSQL

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "test" <<-EOSQL
  CREATE EXTENSION "ltree";
EOSQL
