#!/bin/bash

cp ./.env.default ./.env
cp ./.env.php-fpm.default ./.env.php-fpm

. ./.env.default
. ./.env

docker volume create $PGDATA_VOLUME

make stop # if already runs

if [ "${INSTALLER_PULL_CODE}" == '1' ]
then
  make download
fi
make start
make composer-install
make migrate
