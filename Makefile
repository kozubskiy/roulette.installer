#!make

-include .env.default
-include .env

TS=$(shell date +%s)
MAKE=make

#ifeq ($(APP_USE_BUNDLED_POSTGRES),1)
#	DC=docker-compose -f docker-compose.yml -f docker-compose.traefik.yml -f docker-compose.postgres.yml -f docker-compose.override.yml --compatibility
#else
#	DC=docker-compose -f docker-compose.yml -f docker-compose.traefik.yml -f docker-compose.override.yml --compatibility
#endif

DC=docker-compose -f docker-compose.yml

IS_PROD=
ifeq ($(APP_ENV),prod)
  IS_PROD=1
endif

COMPOSER_NODEV=
ifeq ($(IS_PROD),1)
	COMPOSER_NODEV=--no-dev
endif

# start app containers
start:
	$(DC) up -d

# stop app containers
stop:
	$(DC) down

# restart app containers
restart: stop start

# start containers in modal (not daemonized) mode. Is useful for quick debug.
start-modal:
	$(DC) up

generate-proxies:
	./app/bin/main-doctrine orm:generate-proxies
	./app/bin/audit-doctrine orm:generate-proxies

# first download application code from repository to APP_PATH from APP_REPO:APP_BRANCH
download:
	@test $(INSTALLER_PULL_CODE) || (echo "Nope. Would not download. Configured that repository should not be pulled."; exit 1)
	@echo "Downloading source code (repo $(APP_REPO) branch $(APP_BRANCH))..."
	git clone $(APP_REPO) $(APP_PATH)
	git -C $(APP_PATH) fetch
	git -C $(APP_PATH) checkout $(APP_BRANCH)

# shortcut for initializing application for the first time
init: download start composer-install migrate stop

# pull application code in APP_PATH in current branch
pull:
	git -C $(APP_PATH) pull

# migrate main and audit databases to latest migrations
migrate:
	./app/bin/migrate

# clear application cache
clear-cache:
	rm -rf $(APP_PATH)/var/cache/
	./app/bin/console cache:warmup

# install composer packages to application
composer-install:
	./app/bin/composer install $(COMPOSER_NODEV)
	test "$(IS_PROD)" && ./app/bin/composer dump-env prod

# shortcut used by CI to update application code, composer packages, migrations, clears cache
update: pull composer-install migrate clear-cache generate-proxies restart

# makes databases backup
db-backup:
	./db/bin/pg_dump --username main --no-privileges --no-owner --quote-all-identifiers --format plain --inserts --column-inserts "main" | sed -f "./db/docker/scripts/substitutes.sed" | gzip > ./db/volumes/dumps/main-$$(date +%Y-%m-%d-%H-%M-%S).sql.gz

selfupdate:
	git -C ./ pull
