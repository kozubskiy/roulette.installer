#!/bin/bash

make stop

. ./.env.default
. ./.env

docker volume remove $PGDATA_VOLUME
